/*------------------------------------------------------------------*/
/*  Projet: AVR tiny Threads                                        */
/*  File  : avrTinyThreads.h                                        */
/*  Author: user@W500 greatly inpired by Adam Dunkels      08/28/20 */
/*------------------------------------------------------------------*/
#ifndef __avrTinyThreads_h__
#define __avrTinyThreads_h__
typedef struct AT {
  unsigned short c;
} S_at ;
#define AT_ST_WAIT 0
#define AT_ST_QUIT 1
#define AT_ST_STOP 2
#define AT_SET(AT) (AT)->c = 0 ;
#define AT_BODY(params) char params     // BODY
#define AT_START(AT) {          \
    char AT_YIELD_FLAG = 1;     \
    switch((AT)->c) { case 0:           // START
#define AT_STOP(AT) } ;         \
     AT_YIELD_FLAG = 0;	        \
     AT_SET(AT);                \
     return AT_ST_STOP; }               // STOP
#define AT_QUIT(AT)	        \
  do {			        \
    AT_SET(AT);		        \
    return AT_ST_QUIT;	        \
  } while(0)                            // QUIT
#define AT_RESET(AT)	        \
  do {			        \
    AT_SET(AT);		        \
    return AT_ST_WAIT;	        \
  } while(0)                            // RESET

#define AT_WAIT_UNTIL(AT, exp)  \
  do {			        \
    (AT)->c = __LINE__;         \
    case __LINE__:	        \
    if(!(exp)) {	        \
      return AT_ST_WAIT;        \
    }			        \
  } while(0)                            // UNTIL
#define AT_WAIT_WHILE(AT, expr) \
    AT_WAIT_UNTIL((AT), !(expr))        // WHILE
#define AT_RUN(fct)             \
    ((fct) < AT_ST_QUIT)                // RUN
#define AT_WAIT_THREAD(AT, thread) \
    AT_WAIT_WHILE((AT), AT_RUN(thread)) // WAIT_THREAD

// checked for n calls in a thread : seems OK
#define AT_DELAY(AT, ms)                      \
  do {			                      \
    static unsigned long _AT_ttl ;            \
    _AT_ttl = millis() + (unsigned int)(ms);  \
    AT_WAIT_UNTIL(AT, (millis() > _AT_ttl )); \
  } while(0)                            // DELAY 
//        AT_WAIT_UNTIL(sat, millis() - started > cycleTime );
  #define AT_SYN(AT, origin, ms)                      \
  do {                            \           \
    AT_WAIT_UNTIL(AT, (millis() -  (origin) > (ms) )); \
  } while(0)                            // DELAY 

#endif //  __avrTinyThreads_h__
