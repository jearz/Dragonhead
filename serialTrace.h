/*------------------------------------------------------------------*/
/*  Projet: Dev Tools                                               */
/*  File  : serialTrace.h                                           */
/*  Author: user@W500                                      08/28/20 */
/*------------------------------------------------------------------*/
// RCS  CI/CO : Cx v v,  terminate log : CcCc, view log : Cx v l
// TODO: all this should be in a class or a singleton class
// TODO: strings should be in Flash 32kb code
//       #include <avr/pgmspace.h>

#ifndef __SERIALTRACE_H__
#define __SERIALTRACE_H__
extern char *__brkval;
unsigned long freeMemory() {
  char top;
  return &top - (__brkval ? __brkval : __malloc_heap_start);
}

void retard(unsigned long ms, int mini) {
#ifndef TEST
  delay(ms);
#else
  delay(mini); 
#endif  
}
// functions used for I/O using serial monitor
#ifndef IHM  // if IHM undefined then just empty functions.
// So users don't need to change code, just define/undefine IHM.
// A bit of a waste, but main purpose is testing.
bool ttyAskN     ( String prompt ) {}
bool ttyAskY     ( String prompt ) {}
char ttyGetChar  ( String prompt ) {}
int  ttyGetInt   ( String prompt ) {}
void ttyPutInt   ( String prompt, int num ) {}
void ttyPutMem   ( ) {}
void ttyPutStr   ( String txt ) {}
void ttyPutUin   ( String prompt, unsigned int num ) {}
void ttyPutFloat ( String prompt, float num ) {}
void ttySetIHM   ( long baud ) {}
void ttyWipe     ( short lines ) {}

#else
void ttySetIHM(long baud) {
  Serial.begin(baud); // WIP check if baud in correct values ?
  for (int i=0;i<10;i++) Serial.print("\n\n\n") ; 
}
void ttyPutStr(String txt) {  Serial.println(txt);
}
void ttyPutMem() {
  char buf[32];
  unsigned long octets = freeMemory();
  int k = octets / 1024 ; int o = octets % 1024;
  sprintf(buf, "SRAM: %dKo%d free",k,o);
  Serial.println (buf) ;
}
void ttyPutInt(String prompt, int num ) {
  char buf[64];
  sprintf(buf, "%s %d", prompt.c_str(), num ) ;
  Serial.println (buf) ;
} 
void ttyPutUin(String prompt, unsigned int num ) {
  char buf[64];
  sprintf(buf, "%s %d", prompt.c_str(), num ) ;
  Serial.println(buf) ;
} 
int ttyGetInt(String prompt) {
  Serial.println(prompt);
  while (Serial.available()==0)  {} //Wait for user input  
  String val=Serial.readString();
  return val.toInt();
}
void ttyPutFloat ( String prompt, float num ) {
  Serial.print(prompt); 
  Serial.println (num) ;
  }
void ttyPutFloatRound ( String prompt, float num ) {
  char buf[64];
  sprintf(buf, "%s %d", prompt.c_str(), int(num) ) ;
  Serial.println (buf) ;
  }
char ttyGetChar(String prompt) {
  Serial.println(prompt);  
  while (Serial.available()==0)  {} //Wait for user input  
  return Serial.read();
}
bool ttyAskY(String prompt){
  byte R = 223 & (byte)ttyGetChar(prompt);
  if ('Y' ==  R ) return true;
  else return false ;
}
bool ttyAskN(String prompt){
  byte R = 223 & (byte)ttyGetChar(prompt);
  if ('N' ==  R ) return true;
  return false ;
}
void ttyWipe (short lines) {
//const short ESC=27 ; KO in Arduino serial monitor, works with others tty
//  Serial.write(ESC); Serial.print("[2J"); // cls
//  Serial.write(ESC); Serial.print("[H");  // cursor to home
  while (lines--) Serial.print("\n") ; // somehow overkill !
}
#endif // IHM 
#endif // __SERIALTRACE_H__
